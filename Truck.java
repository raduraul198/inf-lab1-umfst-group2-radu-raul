 class truck extends Car{
    public int weight;

     truck(int speed, double regularPrice, String color) {
         super(speed, regularPrice, color);
     }

     public double getRegularPrice() {
        if(weight>2000){
            return super.getRegularPrice()-(super.getRegularPrice()%10);
        }
        else {
            return super.getRegularPrice() -(super.getRegularPrice()%20);
        }
    }

}
