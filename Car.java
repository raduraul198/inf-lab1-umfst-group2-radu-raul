public class Car {
    public int speed;
    private double regularPrice;
    public  String color;

    public int getSpeed() {
        return speed;
    }

    public String getColor() {
        return color;
    }

    public double getRegularPrice() {
        return regularPrice;
    }
    Car(int speed, double regularPrice, String color){
        this.speed = speed;
        this.regularPrice = regularPrice;
        this.color = color;
    }
    public String toString(){
        return "Viteza este " + speed + "Pretul este : " + getRegularPrice() +"Culoarea " + color;
    }
}