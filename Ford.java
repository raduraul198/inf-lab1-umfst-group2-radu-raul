public class Ford extends Car{
    public int year;
    public int manufacturerDiscount;

    Ford(int speed, double regularPrice, String color) {
        super(speed, regularPrice, color);
    }

    public  double getSalePrice(){
        return super.getRegularPrice() -  (super.getRegularPrice() % manufacturerDiscount);
    }
}
